﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading;
using ZXing.Mobile;
using System.Data.SqlClient;
using System.IO;
namespace AppRestcliente.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Pedidomesa : ContentPage
    {
        public Pedidomesa()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        int idusuario = 0;
        string ruta;
        string cadena_de_conexion;
        string Ip;
        string parte1 = "Data source =";
        string parte2 = ";Initial Catalog=BASEBRIRESTCSHARP;Integrated Security=false;User Id=buman;Password=softwarereal";
        private CancellationTokenSource TimerCancelar;
        int IdMesa=0;
        async public Task <string> EscannerFlash(bool flashOn)
        {
            TimerCancelar = new CancellationTokenSource();
            MobileBarcodeScanner escaneo = new MobileBarcodeScanner();
            escaneo.BottomText = "Escanea el codigo QR de tu mesa";
            ZXing.Result resultado = null;
            CancellationTokenSource controlCancelado = TimerCancelar;
            TimeSpan ts = new TimeSpan(0, 0, 0, 2, 0);
            Device.StartTimer(ts, () =>
             {
                 if(controlCancelado.IsCancellationRequested)
                 {
                     return false;
                 }
                 if(resultado==null)
                 {
                     escaneo.AutoFocus(); 
                     if(flashOn)
                     {
                         escaneo.Torch(true);
                     }
                     return true;
                 }
                 return false;

                 
             });
            resultado = await escaneo.Scan();
            if(resultado!=null)
            {
                await Stop();
                string idCapturado;
                idCapturado = resultado.Text;
                string cadena = idCapturado;
                string[] separadas = cadena.Split('|');
                Ip = separadas[0].ToString();
                string Idprocesado = separadas[1];
                IdMesa =Convert.ToInt32(Idprocesado);
                ValidarConexion();
            }
            await Stop();
            return string.Empty;

        }
        private void PasarDatos()
        {
            if(IdMesa >0)
            {
                Pedidos.idmesa = IdMesa;
                Application.Current.MainPage = new Pedidos();

            }
        }
        async private Task Stop()
        {
            await Task.Run(() =>
            {
                Interlocked.Exchange(ref this.TimerCancelar, new CancellationTokenSource()).Cancel();
            });
        }
        private async void btnEscanear_Clicked(object sender, EventArgs e)
        {
          await EscannerFlash(true);
        }
        private void ProbarConexion()
        {
            cadena_de_conexion = parte1 +Ip+ parte2;
            try
            {
                var conexionmanual = new SqlConnection(cadena_de_conexion);
                conexionmanual.Open();
                var cmd = new SqlCommand("Select Top 1 IdUsuario from Usuarios", conexionmanual);
                idusuario =Convert.ToInt32(cmd.ExecuteScalar());
                conexionmanual.Close();

            }
            catch (Exception)
            {
                idusuario = 0;
            }
        }
        private async void ValidarConexion()
        {
            ProbarConexion();
            if(idusuario >0)
            {
                crear_archivo();
                PasarDatos();
            }
            else
            {
                await DisplayAlert("Sin conexion", "No se logro conectar al servidor", "OK");

            }
        }
        private void crear_archivo()
        {
            ruta = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "connection.txt");
            FileInfo fi = new FileInfo(ruta);
            StreamWriter sw;
            try
            {
                if(File.Exists(ruta)==false)
                {
                    sw = File.CreateText(ruta);
                    sw.WriteLine(parte1 + Ip + parte2);
                    sw.Flush();
                    sw.Close();
                }
                else if (File.Exists(ruta) == true)
                {
                    File.Delete(ruta);
                    sw = File.CreateText(ruta);
                    sw.WriteLine(parte1 + Ip + parte2);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}