﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AppRestcliente.VistaModelo;

namespace AppRestcliente.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Presentacion : ContentPage
    {
        public Presentacion()
        {
            InitializeComponent();
            DependencyService.Get<VMstatusbar>().OcultarStatusBar();
           
        }
        protected override async void OnAppearing()
        {
            await Animacion();
        }
        private async Task Animacion()
        {
            logo.Opacity = 0;
            await logo.FadeTo(1, 3000);
            Application.Current.MainPage =new NavigationPage(new Pedidomesa());

        }

    }
}