﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AppRestcliente.VistaModelo;
using AppRestcliente.Modelo;

namespace AppRestcliente.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Pedidos : ContentPage
    {
        public Pedidos()
        {
            InitializeComponent();
            DependencyService.Get<VMstatusbar>().MostrarStatusBar();
            DependencyService.Get<VMstatusbar>().TemaControlesOscuros();

            Mostrardetalleventa();
            MostrarGrupos();
            MostrarProductos();
            EliminarVentasIncompMovil();
            VerificarVentas();
        }


        public static int idmesa;
        int idventa;
        int idgrupo = 0;
        int idproducto;
        double precioventa;
        string color;
        double total = 0;
        string ventagenerada;
        private async Task MostrarPanelDv()
        {

            uint duracion = 700;
            await Task.WhenAll(
                Panelcontador.FadeTo(0, 500),
                PanelGruposproductos.TranslateTo(0, -540, duracion, Easing.CubicIn),
                PanelDetalleventa.TranslateTo(0,0, duracion, Easing.CubicIn)

                );
            PanelDetalleventa.IsVisible = true;
            DependencyService.Get<VMstatusbar>().TemaControlesClaros();

        }
        private async Task OcultarPanelDv()
        {

            PanelDetalleventa.IsVisible = false;
            uint duracion = 700;
            await Task.WhenAll(
                Panelcontador.FadeTo(1, 500),
                PanelGruposproductos.TranslateTo(0, 0, duracion, Easing.CubicIn),
                PanelDetalleventa.TranslateTo(0, 0, duracion, Easing.CubicIn)

                );

            DependencyService.Get<VMstatusbar>().TemaControlesOscuros();
        }

        private void EliminarVentasIncompMovil()
        {
            var funcion = new VMventas();
            var parametros = new Mventas();
            parametros.Idmesa = idmesa;
            funcion.eliminarVenIncomMovil(parametros);
        }
        private async void btnEnviar_Clicked(object sender, EventArgs e)
        {
            if (total > 0)
            {
                EditarEstadoMesaOcupado();
                EditarEstadoVentasEspera();
                await OcultarPanelDv();
            }

        }
        private void EditarEstadoVentasEspera()
        {
            var funcion = new VMventas();
            var parametros = new Mventas();
            parametros.Idventa = idventa;
            funcion.EditarEstadoVentasEspera(parametros);
        }
        private void EditarEstadoMesaOcupado()
        {
            var funcion = new VMmesas();
            var parametros = new Mmesas();
            parametros.Id_mesa = idmesa;
            funcion.EditarEstadoMesaOcupado(parametros);
        }
        private void Mostrardetalleventa()
        {
            var funcion = new VMdetalleventa();
            var parametros = new Mdetalleventa();
            parametros.idventa = idventa;
            parametros.Idmesa =idmesa;
            var data = funcion.MostrarDetalleVenta(parametros);
            ListaDetalleventa.ItemsSource = data;
            ListaPreviaDv.ItemsSource = data;
            lblContador.Text = data.Count.ToString();
            foreach (var item in data)
            {
                total += item.Importe;
            }
            lblTotal.Text = total.ToString();
        }
        private void MostrarGrupos()
        {
            var funcion = new VMgrupoproductos();
            var dt = funcion.MostrarGrupos();
            Listagrupos.ItemsSource = dt;


        }
        private void MostrarProductos()
        {
            var funcion = new VMproductos();
            var parametros = new Mproductos();
            parametros.Idgrupo = idgrupo;
            var dt = funcion.MostrarProductos(parametros);
            ListaProductos.ItemsSource = dt;
        }

        private void btnGrupo_Clicked(object sender, EventArgs e)
        {
            idgrupo = Convert.ToInt32((sender as Button).CommandParameter);
            MostrarProductos();
        }

        private async void btnagregar_Clicked(object sender, EventArgs e)
        {
            string cadena = ((sender as Button).CommandParameter).ToString();
            string[] separadas = cadena.Split('|');
            idproducto = Convert.ToInt32(separadas[0]);
            precioventa = Convert.ToDouble(separadas[1]);
            color = (separadas[2]).ToString();
            await EfectoAgregarProducto();
            InsertarVentas();

        }
        private void InsertarVentas()
        {
            if (ventagenerada == "VENTA NUEVA")
            {
                var funcion = new VMventas();
                var parametros = new Mventas();
                parametros.Idmesa = idmesa;
                if (funcion.Insertar_ventas(parametros) == true)
                {
                    VerificarVentas();
                }

            }
            if (ventagenerada == "VENTA GENERADA")
            {
                insertarDetalleventa();
            }

        }
        private void insertarDetalleventa()
        {
            var funcion = new VMdetalleventa();
            var parametros = new Mdetalleventa();
            parametros.idventa = idventa;
            parametros.Id_producto = idproducto;
            parametros.cantidad = 1;
            parametros.preciounitario = precioventa;
            parametros.Estado = "EN ESPERA";
            parametros.Costo = 0;
            parametros.Estado_de_pago = "DEBE";
            parametros.Donde_se_consumira = "LOCAL";
            funcion.insertarDetalle_venta(parametros);
            Mostrardetalleventa();

        }

        private void VerificarVentas()
        {
            var funcion = new VMventas();
            var parametros = new Mventas();
            parametros.Idmesa = idmesa;
            funcion.mostrarIdventaMesa(ref idventa, parametros);
            if (idventa > 0)
            {
                ventagenerada = "VENTA GENERADA";
                Mostrardetalleventa();
            }
            else
            {
                ventagenerada = "VENTA NUEVA";
                lblContador.Text = "0";
                lblTotal.Text = "0";
            }
        }

        private async Task EfectoAgregarProducto()
        {
            var caja = new BoxView();
            caja.BackgroundColor = Color.FromHex(color);
            caja.WidthRequest = 40;
            caja.HeightRequest = 40;
            caja.VerticalOptions = LayoutOptions.CenterAndExpand;
            caja.HorizontalOptions = LayoutOptions.CenterAndExpand;
            caja.CornerRadius = 30;
            caja.Scale = 0;

            gridprincipal.Children.Add(caja);
            await caja.ScaleTo(1, 200, Easing.CubicInOut);
            await Task.WhenAll(
                caja.TranslateTo(0, 350, 300),
                caja.ScaleTo(0, 300, Easing.CubicIn)
                );
            gridprincipal.Children.Remove(caja);
        }

        private async void DeslizarOcultarPanelDV(object sender, SwipedEventArgs e)
        {
            await OcultarPanelDv();
        }

        private async void DeslizarMostrarPanelDV(object sender, SwipedEventArgs e)
        {
            await MostrarPanelDv();
        }
    }
}