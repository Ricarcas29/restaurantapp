﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AppRestcliente.Modelo;
using AppRestcliente.Conexion;
namespace AppRestcliente.VistaModelo
{
   public class VMproductos
    {
        public List<Mproductos> MostrarProductos(Mproductos parametrosPedir)
        {
            var grupos = new List<Mproductos>();
            DataTable dt = new DataTable();
            CONEXIONMAESTRA.abrir();
            var proceso = new SqlDataAdapter("mostrar_Productos_por_grupo", CONEXIONMAESTRA.conectar);
            proceso.SelectCommand.CommandType = CommandType.StoredProcedure;
            proceso.SelectCommand.Parameters.AddWithValue("@id_grupo", parametrosPedir.Idgrupo);
            proceso.SelectCommand.Parameters.AddWithValue("@buscador", "");
            proceso.Fill(dt);
            foreach (DataRow rdr in dt.Rows)
            {
                var parametros = new Mproductos();
                parametros.Descripcion = rdr["Descripcion"].ToString();
                parametros.Idproducto = Convert.ToInt32(rdr["Id_Producto1"]);
                parametros.ColorHtml = rdr["ColorHtml"].ToString();
                parametros.Moneda = Convert.ToInt32(rdr["Id_Producto1"]) + "|" + rdr["Precio_de_venta"].ToString() + "|" + rdr["ColorHtml"].ToString() + "|" + rdr["Moneda"].ToString();
                parametros.Precio = rdr["Precio_de_venta"].ToString();     
                grupos.Add(parametros);

            }
            return grupos;
        }
    }
}
