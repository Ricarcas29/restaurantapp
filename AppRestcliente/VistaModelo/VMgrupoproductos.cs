﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AppRestcliente.Modelo;
using AppRestcliente.Conexion;

namespace AppRestcliente.VistaModelo
{
   public class VMgrupoproductos
    {
        public List<Mgrupos> MostrarGrupos()
        {
            var grupos = new List<Mgrupos>();
            DataTable dt = new DataTable();
            CONEXIONMAESTRA.abrir();
            var proceso = new SqlDataAdapter("mostrarGruposProd", CONEXIONMAESTRA.conectar);
            proceso.Fill(dt);
            foreach (DataRow rdr in dt.Rows)
            {
                var parametros = new Mgrupos();
                parametros.Grupo = rdr["Grupo"].ToString();
                parametros.Idgrupo = Convert.ToInt32(rdr["Idline"]);
                parametros.ColorHtml = rdr["ColorHtml"].ToString();
                grupos.Add(parametros);

            }
            return grupos;

        }
    }
}
