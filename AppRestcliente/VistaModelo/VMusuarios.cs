﻿using System;
using System.Collections.Generic;
using System.Text;
using AppRestcliente.Conexion;
using System.Data.SqlClient;

namespace AppRestcliente.VistaModelo
{
  public  class VMusuarios
    {
        public void ObtenerIdusercliente(ref int idusuario)
        {
            try
            {
                CONEXIONMAESTRA.abrir();
                SqlCommand da = new SqlCommand("select IdUsuario from Usuarios where Rol ='Cliente'", CONEXIONMAESTRA.conectar);
                idusuario = Convert.ToInt32(da.ExecuteScalar());
            }
            catch (Exception ex)
            {
                idusuario = 0;
            }
            finally
            {
                CONEXIONMAESTRA.cerrar();
            }
        }
    }
}
