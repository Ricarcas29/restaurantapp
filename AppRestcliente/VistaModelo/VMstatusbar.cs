﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppRestcliente.VistaModelo
{
    public interface VMstatusbar
    {
        void OcultarStatusBar();
        void MostrarStatusBar();
        void TemaControlesOscuros();
        void TemaControlesClaros();
    }
}
