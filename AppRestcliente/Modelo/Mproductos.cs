﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppRestcliente.Modelo
{
   public class Mproductos
    {
        public int Idproducto { get; set; }
        public int Idgrupo { get; set; }
        public string Descripcion { get; set; }
        public string Precio { get; set; }
        public string ColorHtml { get; set; }
        public string Grupo { get; set; }
        public string Moneda { get; set; }
    }
}
