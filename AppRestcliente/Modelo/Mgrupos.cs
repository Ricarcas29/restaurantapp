﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppRestcliente.Modelo
{
   public class Mgrupos
    {
        public int Idgrupo { get; set; }
        public string Grupo { get; set; }
        public string ColorHtml { get; set; }
    }
}
